#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <error.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include "decodeg3.h"

#define errexit(val) do { if (val == -1) { error_at_line(val, errno, __FILE__, __LINE__, ""); } } while (false)

int
getbit(char* block, int* index)
{
  int bitnr = *index & 0b0111;
  int bytenr = (*index & ~0b0111) >> 3;
  int byte = block[bytenr];
  int bit = (byte >> (7-bitnr)) & 1;

  (*index)++;

  return bit;
}

int
sym_lookup(int sym, short len, bool white)
{
  struct code* table;

  if (len > maxlen)
    return CODE_CORRUPT;
  if (len < (white ? minlen_white : minlen_black))
    return CODE_NOPE;

  table = white ? whites : blacks;

  int end = white ? white_len : black_len;
  for (int i = 0; i < end; i++)
    if ((table[i].bits == sym) && (table[i].len == len)) return table[i].encoded;

  return CODE_NOPE;
}

int
get_sym(char* block, int* index, bool white)
{
  int sym=0, fill=0, foundsym;
  short len = 0;

  do {
    sym = (sym << 1) | getbit(block, index);
    /* eat consecutive runs of 0x00, which are fill bits.  can't just
     * return a "fill" code because they can be arbitrarily long.
     * this means, in essence, that the EOL code is "at least 11 zeros
     * followed by one 1" 
     */
    if ((sym == 0) && (len == 11))
      len--, fill++;
    if (fill > 1024)
      return CODE_CORRUPT;
  } while ((foundsym = sym_lookup(sym, ++len, white)) == CODE_NOPE);

  return foundsym;
}

int get_runlen(char* block, int* index, bool white)
{
  int run = 0, sym;
  do {
    sym = get_sym(block, index, white);
    if (sym < 0) return sym;
    run += sym;
  } while (sym >= 64);
  return run;
}

int get_line(char* block, int* index, int maxlen, 

int
test(void)
{
  int index = 0;
  int sym = 0;
  printf("getting some bits\n");

  while (index < 16)
    printf("%d", getbit(testbuf, &index));
  puts("");


  puts("testing symbols");
  index = 0;
  printf("symbol % 5d is % 5d (should be 00001011 = 48)\n",
	 sym++, get_sym(testbuf, &index, true));
  printf("symbol % 5d is % 5d (should be 00000111 = 14)\n",
	 sym++, get_sym(testbuf, &index, false));
  printf("symbol % 5d is % 5d (should be 0111 = 2)\n",
	 sym++, get_sym(testbuf, &index, true));
  printf("symbol % 5d is % 5d (should be 00011 = 7)\n",
	 sym++, get_sym(testbuf, &index, false));
  printf("symbol % 5d is % 5d (should be 011000 = 1664)\n",
	 sym++, get_sym(testbuf, &index, true));
  printf("symbol % 5d is % 5d (should be 0001100 = 19)\n",
	 sym++, get_sym(testbuf, &index, true));
  printf("symbol % 5d is % 5d (should be 000000000001 = -1)\n",
	 sym++, get_sym(testbuf, &index, false));

  puts("testing runs");
  sym = 0; index = 0;
  printf("run % 5d is % 5d (should be % 5d)\n",
	 sym++, get_runlen(testbuf, &index, true), 48);
  printf("run % 5d is % 5d (should be % 5d)\n",
	 sym++, get_runlen(testbuf, &index, false), 14);
  printf("run % 5d is % 5d (should be % 5d)\n",
	 sym++, get_runlen(testbuf, &index, true), 2);
  printf("run % 5d is % 5d (should be % 5d)\n",
	 sym++, get_runlen(testbuf, &index, false), 7);
  printf("run % 5d is % 5d (should be % 5d)\n",
	 sym++, get_runlen(testbuf, &index, true), 1664+19);
  printf("run % 5d is % 5d (should be % 5d)\n",
	 sym++, get_runlen(testbuf, &index, false), CODE_EOL);

  index = 0;
}

int
main(int argc, char** argv)
{
  char* inbuf;
  ssize_t inlen = 1728*10000;
  int infile, outfile;

  inbuf = malloc(inlen);
  if (inbuf == NULL) return -1;

  if (argc < 1) {
    puts("need an input file");
    return -1;
  }
  if (argc < 2) {
    puts("need an output file");
    return -1;
  }

  infile = open(argv[1], O_RDONLY);
  errexit(infile);
  outfile = open(argv[2], O_WRONLY | O_CREAT);
  errexit(outfile);
  inlen = read(infile, inbuf, (size_t)inlen);
  errexit(inlen);

  
}

