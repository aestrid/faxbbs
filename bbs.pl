#!/usr/bin/perl -w

#apt-get install libexpect-perl
use Expect;

#use IPC::Open2;
#open2( \*MODEM_IN, \*MODEM_OUT, "cu -l/dev/ttyUSB0 2>&1");

my $e = Expect->spawn('/usr/bin/cu', '-l/dev/ttyUSB0', '-s19200', 
		      '--escape', '');

sleep(2);

# reset modem
print $e "ATZ\r\n";
$e->expect(10, "OK");

print $e "ATH\r\n";
$e->expect(10, "OK");

# go into fancyfax mode
print $e "AT+FCLASS=2.0\r\n";
$e->expect(1, "OK");

# don't even try to allow the far end to go into data mode.  fuck
# completely off with that computer bullshit.
print $e "AT+FAA=0\r\n";
$e->expect(1, "OK");

# tell me alllll about image exchange.  but not nonstandard
# negotiation frames, i don't want to hear about any of your weird
# kinks.  consenting adults and all that.
print $e "AT+FNR=1,1,1,0\r\n";
$e->expect(1, "OK");

# bit-ordering and eol alignment
print $e "AT+FBO=1+FEA=0\r\n";
$e->expect(1, "OK");

# hardware handshaking
print $e "AT+FLO=2\r\n";
$e->expect(1, "OK");

print $e "AT+FLI=\"Shadytel Fax BBS\"\r\n";
$e->expect(1, "OK");

while (1) {
    print $e "\r\n";
    sleep(1);
    print $e "ATH\r\n";
    $e->expect(10, "OK");

    # yes of course we have pixels waiting, if you want them.
    # also we can receive, i guess.
    #
    # unfortunately the calling station (i.e., not the bbs) gets to
    # decide the mode, and we have zero input on it :(
    print $e "AT+FLP=1+FCR=1\r\n";
    $e->expect(1, "OK");

    # when the phone rings, answer it!! but don't try to be a fax yet
    $e->expect(undef, "RING");
    print $e "ATH1\r\n";
    $e->expect(1, "OK");
    # some kind of dialprompt: dtmf 44
    print $e "ATX0DT44;\r\n";
    $e->expect(3, "OK");

    $e->clear_accum();
    print $e "ATH1%T\r";
    $e->expect(10, "-re", qr/[*0-9abcd]*#/i);
    my $numba = $e->match();
    print "match: $numba aa\n";
    #sleep(10);
    #$e->expect(0);
    #my $numba = $e->before();
    print $e "\r\n";
    $e->expect(1, "OK");


}


